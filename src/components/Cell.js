import React, { useState } from "react";

const Cell = (props) => {

	const [value, setValue] = useState(props.value);

	const handleChange = (event) => {
		setValue(event.target.value);

		props.updateValue(props.name, event.target.value);
	};

	return (
		<td className={props.readOnly ? "noteditable" : "editable"}>
			<input
				type="text"
				value={value}
				readOnly={props.readOnly}
				onChange={(event) => handleChange(event)}
				placeholder="edit..."
			/>
		</td>
	);
};

export default Cell;
