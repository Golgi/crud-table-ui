import React, { useState } from "react";
import Cell from "./Cell";

const Row = (props) => {

	const [data, setData] = useState(props.data);
	const [readOnly, setReadOnly] = useState(props.readOnly);

	const updateValue = (key, value) => {
		const cData = data;
		cData[key] = value;
		setData(cData);
	};

	const handleClickDelete = () => {
		props.deleteObj(props.index, data);
	};

	const handleClickEdit = () => {
		setReadOnly(!readOnly);

		if (!readOnly) {
			if (!props.isNew) {
				props.updateObj(props.index, data);
			} else {
				props.putFunction(data);
			}
		}
	};

	const cData = props.data;
	let cols = [];

	for (let [key, value] of Object.entries(cData)) {
		let val = value;
		if (typeof value !== "string" && typeof value !== "number") {
			val = "";
		}
		cols.push(
			<Cell
				key={key}
				value={val}
				name={key}
				readOnly={readOnly}
				updateValue={(key, value) => updateValue(key, value)}
			/>
		);
	}

	let buttonText = readOnly ? "Edit" : "Save";
	buttonText = props.isNew ? "Put" : buttonText;
	
	return (
		<tr>
			{cols}
			<td>
				<button onClick={() => handleClickDelete()}>delete</button>
			</td>
			<td>
				<button onClick={() => handleClickEdit()}>{buttonText}</button>
			</td>
		</tr>
	);
};

export default Row;
